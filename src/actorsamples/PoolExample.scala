package actorsamples

import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.routing.BalancingPool
import java.awt.image.BufferedImage
//import swing._
import java.awt.Color

object PoolExample extends App {
  val MaxCount = 10000
  val ImageSize = 500

  case class Complex(real: Double, imag: Double) {
    def +(c: Complex) = Complex(real + c.real, imag + c.imag)
    def *(c: Complex) = Complex(real * c.real - imag * c.imag, real * c.imag + imag * c.real)
    def mag = math.sqrt(real * real + imag * imag)
  }

  def mandelStep(z: Complex, c: Complex): Complex = z * z + c

  def mandelCount(c: Complex): Int = {
    var cnt = 0
    var z = c
    while (cnt < MaxCount && z.mag < 4) {
      z = mandelStep(z, c)
      cnt += 1
    }
    cnt
  }

  case class MakeImage(xmin: Double, xmax: Double, ymin: Double, ymax: Double)
  case class Line(row: Int, y: Double, xmin: Double, xmax: Double)
  case class LineResult(row: Int, rgbs: Array[Int])

  val img = new BufferedImage(ImageSize, ImageSize, BufferedImage.TYPE_INT_RGB)
//  val panel = new Panel {
//    override def paint(g: Graphics2D): Unit = {
//      g.drawImage(img, 0, 0, null)
//    }
//    preferredSize = new Dimension(ImageSize, ImageSize)
//  }

  class MandelActor(router:ActorRef) extends Actor {
    def receive = {
      case MakeImage(xmin, xmax, ymin, ymax) =>
        for(i <- 0 until ImageSize) {
          val y = ymin + i*(ymax-ymin)/ImageSize
          router ! Line(i,y,xmin,xmax)
        }
    }
  }

  class LineActor(mandel:ActorRef) extends Actor {
    def receive = {
      case Line(r, y, xmin, xmax) =>
        mandel ! LineResult(r, Array.tabulate(ImageSize)(i => {
          val x = xmin + i * (xmax - xmin) / ImageSize
          val cnt = mandelCount(Complex(x, y))
          new Color(cnt.toFloat/MaxCount,0f,0f).getRGB
        }))
    }
  }
  
  val start = System.nanoTime
  class ImageActor extends Actor {
    var waiting = (0 until ImageSize).toSet
    def receive = {
      case LineResult(r, rgbs) =>
      	for(i <- rgbs.indices) img.setRGB(i,r,rgbs(i))
      	waiting -= r
      	if(waiting.isEmpty) {
//      	  panel.repaint
      	  println("Time = "+(System.nanoTime()-start)/1e9)
      	}
    }
  }

  val system = ActorSystem("Mandelbrot")
  val imageActor = system.actorOf(Props[ImageActor], "ImageActor")
  val router = system.actorOf(BalancingPool(4).props(Props(new LineActor(imageActor))),"poolRouter")
  val mandelActor = system.actorOf(Props(new MandelActor(router)), "MandelActor")
  
  mandelActor ! MakeImage(-1.5,0.5,-1,1)

//  val frame = new MainFrame {
//    title = "Mandelbrot Set"
//    contents = panel
//  }
//
//  frame.open

}
package actorsamples

import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.Actor

object SimpleExample extends App {
  class SimpleActor extends Actor {
    var number = 0
    def receive = {
      case s:String => println("String "+s)
      case i:Int => println("Int "+i)
        number = i
      case _ => println("Unknown message")
    }
    def foo():String = { number = 10; "Done!" }
  }
  
  val system = ActorSystem("SimpleExample")
  val actor = system.actorOf(Props[SimpleActor],"FirstActor")
  
  actor ! "Hi"
  println("Hi sent")
  actor ! 42
  println("42 sent")
  actor ! 'a'
  println("a sent")
  
  system.shutdown
}
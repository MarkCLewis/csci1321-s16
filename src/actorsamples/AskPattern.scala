package actorsamples

import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.Props
import akka.pattern.Patterns._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

object AskPattern extends App {
  case object AskName
  
  class AskActor(val name:String) extends Actor {
    def receive = {
      case AskName =>
        val s = sender
        s ! name
    }
  }
  
  val system = ActorSystem("SimpleExample")
  val actor = system.actorOf(Props(new AskActor("Bob")),"NamedActor")
//  import ExecutionContext.Implicits.global
  implicit val ec = system.dispatcher
  
  val nameFuture = ask(actor,AskName,1.seconds)
//  actor ? AskName

  nameFuture.onSuccess {
    case s => println("name = "+s)
  }
//  println(Await.result(nameFuture,1000.millis))
  
  system.shutdown
}
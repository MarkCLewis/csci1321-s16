

/**
 * @author mlewis
 */
object HelloWorld {
  def main(args:Array[String]):Unit = {
    println("Hello World!")
    println(System.in.available())
    Thread.sleep(2000)
    println(System.in.available())
  }
}
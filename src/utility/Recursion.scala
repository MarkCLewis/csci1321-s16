package utility

/**
 * @author mlewis
 */
object Recursion extends App {
  def fact(n:Int):Int = if(n<2) 1 else n*fact(n-1)
  
  def fib(n:Int):Int = if(n<2) 1 else fib(n-1)+fib(n-2)
  
  println(fact(5))
  println(fib(5))
  
  val people = List(4,8,2,5,9,8,7,4,1)
  
  def minDiff(p:List[Int], diff:Int):Int = {
    if(p.isEmpty) {
      diff.abs
    } else {
      val onTeamA = minDiff(p.tail, diff+p.head)
      if(onTeamA == 0) 0 else {
        val onTeamB = minDiff(p.tail, diff-p.head)
        onTeamA min onTeamB
      }
    }
  }
  
  println(minDiff(people.tail,people.head))
  
  val graph = Array(Array(0,1,0,0,0,0),
                    Array(0,0,1,0,0,0),
                    Array(0,0,0,1,1,0),
                    Array(0,1,0,0,1,0),
                    Array(0,0,0,0,0,0),
                    Array(0,0,1,0,0,0))
                    
  def reachable(n1:Int, n2:Int, g:Array[Array[Int]], 
      visited:Set[Int]):Boolean = {
    if(n1 == n2) true
    else if(visited(n1)) false
    else {
      var ret = false
      var i = 0
      while(i < g.length && !ret) {
        if(g(n1)(i) > 0) ret ||= reachable(i, n2, g, visited+n1)
        i += 1
      }
      ret
//      (0 until g.length).exists(i => {
//        g(n1)(i) > 0 && reachable(i, n2, g, visited+n1)
//      })
    }
  }
  
  def shortestPathLength(n1:Int, n2:Int, g:Array[Array[Int]], 
      visited:Set[Int]):Int = {
    if(n1 == n2) 0
    else if(visited(n1)) 1000000000
    else {
      var ret = 1000000000
      for(i <- 0 until g.length) {
        if(g(n1)(i) > 0) ret = ret min (1+shortestPathLength(i, n2, g, visited+n1))
      }
      ret
    }
  }
  
  def shortestPath(n1:Int, n2:Int, g:Array[Array[Int]], 
      visited:Set[Int]):List[Int] = {
    if(n1 == n2) List(n2)
    else if(visited(n1)) Nil
    else {
      val ret = for(i <- 0 until g.length) yield {
        if(g(n1)(i) > 0) {
          val sp = shortestPath(i, n2, g, visited+n1)
          if(sp.isEmpty) Nil else n1 :: sp
        } else {
          Nil
        }
      }
      val nonEmpty = ret.filter(_.nonEmpty)
      if(nonEmpty.isEmpty) Nil else nonEmpty.minBy { x => x.length }
    }
  }
}
package utility

/**
 * @author mlewis
 */
class Formula2(val expression: String) {
  val root = Formula2.parse(expression)
  def apply(vars:Map[String,Double]):Double = root(vars)
}

object Formula2 {
  def parse(expression: String): Node = {
    val expr = expression.trim
    var opLoc = -1
    var i = expr.length - 1
    var parensCount = 0
    while (i > 0) {
      if (expr(i) == '(') parensCount += 1
      else if (expr(i) == ')') parensCount += 1
      else if (parensCount == 0 && (expr(i) == '+' || expr(i) == '-')) {
        opLoc = i
        i = -1
      } else if (parensCount == 0 && (expr(i) == '*' || expr(i) == '/' && opLoc < 0)) {
        opLoc = i
      }
      i -= 1
    }
    if (opLoc < 0) {
      if (expr.startsWith("(")) parse(expr.substring(1, expr.length - 1))
      else try {
        NumberNode(expr.toDouble)
      } catch {
        case e:NumberFormatException => VariableNode(expr)
      }
    } else {
      expr(opLoc) match {
        case '+' => BinaryOpNode(parse(expr.substring(0, opLoc)), parse(expr.substring(opLoc + 1)), _+_)
        case '-' => BinaryOpNode(parse(expr.substring(0, opLoc)), parse(expr.substring(opLoc + 1)), _-_)
        case '*' => BinaryOpNode(parse(expr.substring(0, opLoc)), parse(expr.substring(opLoc + 1)), _*_)
        case '/' => BinaryOpNode(parse(expr.substring(0, opLoc)), parse(expr.substring(opLoc + 1)), _/_)
      }
    }
  }

  trait Node {
    def apply(vars: Map[String, Double]): Double
  }
  case class NumberNode(x:Double) extends Node {
    def apply(vars: Map[String, Double]): Double = x
  }
  case class VariableNode(name:String) extends Node {
    def apply(vars: Map[String, Double]): Double = vars(name)
  }
  case class BinaryOpNode(left:Node, right:Node, op: (Double, Double) => Double) extends Node {
    def apply(vars: Map[String, Double]): Double = {
      op(left(vars), right(vars))
    }
  }

  def main(args: Array[String]): Unit = {
    val vars = Map("x" -> 5.0, "y" -> 10.0)
    println(parse("3+4*x")(vars))
    println(parse("(3+4)*x")(vars))
    val form = new Formula2("5*3-x/6+37*y*y")
    for(x <- 1.0 to 10.0 by 1.0) {
      println(form(vars+ ("x" -> x)))
    }
  }
}
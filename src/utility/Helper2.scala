package utility

/**
 * @author mlewis
 */
object Helper2 extends App {
  def findAndRemove[A](lst: List[A])(pred: A => Boolean): (Option[A], List[A]) = {
    lst match {
      case Nil => (None, Nil)
      case h::t =>
        if(pred(h)) (Some(h), t)
        else {
          val (o, nonMatches) = findAndRemove(t)(pred)
          (o, h::nonMatches)
        }
    }
  }
}
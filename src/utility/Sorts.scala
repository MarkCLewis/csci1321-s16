package utility

import scalafx.scene.paint.Color

/**
 * @author mlewis
 */
object Sorts extends App {
  def bubbleSort(a: Array[Int]): Unit = {
    for (
      i <- 0 until a.length;
      j <- 0 until a.length - 1 - i
    ) {
      if (a(j) > a(j + 1)) {
        val tmp = a(j)
        a(j) = a(j + 1)
        a(j + 1) = tmp
      }
    }
  }

  def bubbleSort2[A <% Ordered[A]](a: Array[A]): Unit = {
    for (
      i <- 0 until a.length - 1;
      j <- 0 until a.length - 1 - i
    ) {
      if (a(j) > a(j + 1)) {
        val tmp = a(j)
        a(j) = a(j + 1)
        a(j + 1) = tmp
      }
    }
  }

  def bubbleSort3[A](a: Array[A])(gt: (A, A) => Boolean): Unit = {
    for (
      i <- 0 until a.length;
      j <- 0 until a.length - 1 - i
    ) {
      if (gt(a(j), a(j + 1))) {
        val tmp = a(j)
        a(j) = a(j + 1)
        a(j + 1) = tmp
      }
    }
  }

  private def merge[A](l1: List[A], l2: List[A])(lt: (A, A) => Boolean): List[A] = {
    (l1, l2) match {
      case (Nil, _) => l2
      case (_, Nil) => l1
      case (h1 :: t1, h2 :: t2) =>
        if (lt(h1, h2)) h1 :: merge(t1, l2)(lt)
        else h2 :: merge(l1, t2)(lt)
    }
  }

  def mergeSort[A](lst: List[A])(lt: (A, A) => Boolean): List[A] = {
    val (front, back) = lst.splitAt(lst.length / 2)
    merge(mergeSort(front)(lt), mergeSort(back)(lt))(lt)
  }

  def quicksort[A](lst: List[A])(lt: (A, A) => Boolean): List[A] = lst match {
    case Nil => lst
    case h::Nil => lst
    case _ =>
      val (less, greater) = lst.tail.partition(a => lt(a, lst.head))
      quicksort(less)(lt) ::: (lst.head :: quicksort(greater)(lt))
  }

  def quicksort[A](a: Array[A], start: Int, end: Int)(lt: (A, A) => Boolean): Unit = {
    if (end > start + 1) {
      val r = util.Random.nextInt(end - start) + start
      val tmp = a(r)
      a(r) = a(start)
      a(start) = tmp
      var low = start + 1
      var high = end - 1
      while (high >= low) {
        if (lt(a(low), a(start))) low += 1
        else {
          val tmp = a(low)
          a(low) = a(high)
          a(high) = tmp
          high -= 1
        }
      }
      val tmp2 = a(start)
      a(start) = a(high)
      a(high) = tmp2
      quicksort(a, start, high)(lt)
      quicksort(a, low, end)(lt)
    }
  }

  val nums = Array.fill(10)(util.Random.nextInt(100))
  println(nums.mkString(", "))
  bubbleSort3(nums)(_ % 10 > _ % 10)
  println(nums.mkString(", "))

  val colors = Array.fill(10)(Color(math.random, math.random, math.random, 1.0))
  bubbleSort3(colors)(_.red > _.red)
  colors.foreach(println)
}
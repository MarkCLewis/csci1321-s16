package utility

/**
 * @author mlewis
 */
object Helper {
  def findAndRemove[A](lst: List[A])(pred: A => Boolean): (Option[A], List[A]) = {
    lst match {
      case Nil => (None, Nil)
      case h::t =>
        if(pred(h)) (Some(h), t)
        else {
          val (m, r) = findAndRemove(t)(pred)
          (m, h::r)
        }
    }
  }
}
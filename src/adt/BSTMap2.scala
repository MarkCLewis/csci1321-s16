package adt

import collection.mutable

/**
 * @author mlewis
 */
class BSTMap2[K, V](comp: (K, K) => Int) extends mutable.Map[K, V] {
  import BSTMap2._

  private var root: Node[K, V] = null

  def get(key: K): Option[V] = {
    if (root == null) None else {
      var rover = root
      var c = comp(key, rover.key)
      while (rover != null && c != 0) {
        if (c < 0) rover = rover.left
        else rover = rover.right
        c = comp(key, rover.key)
      }
      if (rover == null) None else Some(rover.value)
    }
  }

  def iterator = new Iterator[(K, V)] {
    val stack = new ListStack[Node[K,V]]
    def runLeft(n:Node[K,V]):Unit = {
      var rover = n
      while(rover!=null) {
        stack.push(rover)
        rover = rover.left
      }
    }
    runLeft(root)
    def next:(K,V) = {
      val n = stack.pop()
      runLeft(n.right)
      (n.key, n.value)
    }
    def hasNext:Boolean = !stack.isEmpty
  }

  def +=(kv: (K, V)): BSTMap2.this.type = {
    val (k, v) = kv
    if (root == null) {
      root = new Node(k, v, null, null)
    } else {
      var rover = root
      var c = comp(k, rover.key)
      while (rover != null && c != 0) {
        if (c < 0) {
          if (rover.left != null) rover = rover.left
          else {
            rover.left = new Node(k, v, null, null)
            rover = null
          }
        } else {
          if (rover.right != null) rover = rover.right
          else {
            rover.right = new Node(k, v, null, null)
            rover = null
          }
        }
        if (rover != null) c = comp(k, rover.key)
      }
      if (rover != null) rover.value = v
    }
    this
  }

  def -=(key: K) = {
    ???
    this
  }

  def inOrder(n: Node[K, V], f: V => Unit): Unit = {
    if (n != null) {
      inOrder(n.left, f)
      f(n.value)
      inOrder(n.right, f)
    }
  }
  def preOrder(n: Node[K, V], f: V => Unit): Unit = {
    if (n != null) {
      f(n.value)
      preOrder(n.left, f)
      preOrder(n.right, f)
    }
  }
  def postOrder(n: Node[K, V], f: V => Unit): Unit = {
    if (n != null) {
      postOrder(n.left, f)
      postOrder(n.right, f)
      f(n.value)
    }
  }
}

object BSTMap2 extends App {
  private class Node[K, V](val key: K, var value: V, var left: Node[K, V], var right: Node[K, V])

  val bst = new BSTMap2[Int, Int](_ - _)
  bst ++= List(6, 2, 8, 1, 9, 7, 5, 3, 4).map(i => i -> i)
  println(bst(6))
  println(bst(1))
  println(bst(9))
  for(t <- bst) println(t)
}
package adt

/**
 * @author mlewis
 */
class DLinkedList[A] {
  private var default: A = _

  private class Node(var prev: Node, val data: A, var next: Node)
  private val end = new Node(null, default, null)
  end.next = end
  end.prev = end
  var numElems = 0

  def add(elem: A, index: Int): Unit = {
    assert(index >= 0 && index <= numElems)
    var rover = end.next
    for (i <- 0 until index) rover = rover.next
    val n = new Node(rover.prev, elem, rover)
    rover.prev.next = n
    rover.prev = n
    numElems += 1
  }

  def remove(index: Int): A = {
    assert(index >= 0 && index <= numElems)
    var rover = end.next
    for (i <- 0 until index) rover = rover.next
    val tmp = rover.data
    rover.prev.next = rover.next
    rover.next.prev = rover.prev
    numElems -= 1
    tmp
  }

  def apply(index: Int): A = {
    assert(index >= 0 && index < numElems)
    var rover = end.next
    for (i <- 0 until index) rover = rover.next
    rover.data
  }

  def length: Int = {
    numElems
  }
  
}
package adt

/**
 * @author mlewis
 */
trait MyStack[A] {
  def push(o: A): Unit
  def pop(): A
  def peek: A
  def isEmpty: Boolean
}
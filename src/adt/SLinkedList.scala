package adt

/**
 * @author mlewis
 */
class SLinkedList[A] {
  private class Node(val data: A, var next: Node)
  private var head: Node = null

  def add(elem: A, index: Int): Unit = {
    assert(index >= 0)
    if(index == 0) {
      head = new Node(elem, head) 
    } else {
      var rover = head
      for(i <- 0 until index-1) rover = rover.next
      rover.next = new Node(elem, rover.next)
    }
  }
  
  def remove(index: Int): A = {
    assert(index >= 0)
    if(index == 0) {
      val tmp = head.data
      head = head.next 
      tmp
    } else {
      var rover = head
      for(i <- 0 until index-1) rover = rover.next
      val tmp = rover.next.data
      rover.next = rover.next.next
      tmp
    }
  }
  
  def apply(index: Int): A = {
    assert(index >= 0)
    var rover = head
    for(i <- 0 until index) rover = rover.next
    rover.data
  }
  
  def length: Int = {
    var i = 0
    var rover = head
    while(rover != null) {
      i += 1
      rover = rover.next
    }
    i
  }

}
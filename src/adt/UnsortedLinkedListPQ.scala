package adt

/**
 * @author mlewis
 */
class UnsortedLinkedListPQ[A](higherP: (A,A) => Boolean) 
    extends MyPriorityQueue[A] {
  private var default: A = _
  private class Node(val data: A, var prev: Node, var next: Node)
  private val end = new Node(default, null, null)
  end.prev = end
  end.next = end

  def enqueue(o: A): Unit = {
    val n = new Node(o, end, end.next)
    end.next.prev = n
    end.next = n
  }
  def dequeue(): A = {
    val n = highestPriorityNode
    n.prev.next = n.next
    n.next.prev = n.prev
    n.data
  }
  def peek: A = highestPriorityNode.data
  def isEmpty: Boolean = end.next == end
  
  private def highestPriorityNode:Node = {
    var tmp = end.next
    var rover = end.next.next
    while(rover!=end) {
      if(higherP(rover.data, tmp.data)) tmp = rover
      rover = rover.next
    }
    tmp
  }
}
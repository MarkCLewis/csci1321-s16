package adt

/**
 * @author mlewis
 */
trait MyQueue[A] {
  def enqueue(o: A): Unit
  def dequeue(): A
  def peek: A
  def isEmpty: Boolean
}
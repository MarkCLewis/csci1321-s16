package adt

/**
 * @author mlewis
 */
class ListQueue[A] extends MyQueue[A] {
  private class Node(val data: A, var next: Node)
  private var front: Node = null
  private var back: Node = null

  def enqueue(o: A): Unit = {
    back.next = new Node(o, null)
    back = back.next
    if (front == null) front = back
  }
  def dequeue(): A = {
    val tmp = front.data
    front = front.next
    if (front == null) back = null
    tmp
  }
  def peek: A = front.data
  def isEmpty: Boolean = front == null
}
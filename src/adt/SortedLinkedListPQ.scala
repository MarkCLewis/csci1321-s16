package adt

/**
 * @author mlewis
 */
class SortedLinkedListPQ[A](higherP: (A, A) => Boolean)
    extends MyPriorityQueue[A] {
  private class Node(val data: A, var next: Node)
  private var head: Node = null

  def enqueue(o: A): Unit = {
    if (head == null || higherP(o, peek)) {
      head = new Node(o, head)
    } else {
      var rover = head
      while (rover.next != null && higherP(rover.next.data, o)) {
        rover = rover.next
      }
      rover.next = new Node(o, rover.next)
    }
  }
  def dequeue(): A = {
    val tmp = peek
    head = head.next
    tmp
  }
  def peek: A = head.data
  def isEmpty: Boolean = head == null
}
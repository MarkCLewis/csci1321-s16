package adt

import scala.reflect.ClassTag

/**
 * @author mlewis
 */
class ArrayStack[A: ClassTag] extends MyStack[A] {
  private var data = new Array[A](10)
  private var top = 0
  
  def push(o: A): Unit = {
    if(top >= data.length) {
      val tmp = new Array[A](data.length*2)
      for(i <- 0 until top) tmp(i) = data(i)
      data = tmp
    }
    data(top) = o
    top += 1
  }
  def pop(): A = {
    top -= 1
    data(top)
  }
  def peek: A = data(top-1)
  def isEmpty: Boolean = top == 0

}
package adt

import scala.reflect.ClassTag

/**
 * @author mlewis
 */
class SortedArrayPQ[A : ClassTag](higherP: (A,A) => Boolean)
    extends MyPriorityQueue[A] {
  private var top = 0
  private var data = new Array[A](10)

  def enqueue(o: A): Unit = {
    // TODO - grow if too big
    var i = top-1
    while(i >= 0 && higherP(data(i),o)) {
      data(i+1) = data(i)
      i -= 1
    }
    data(i+1) = o
    top += 1
  }
  def dequeue(): A = {
    top -= 1
    data(top)
  }
  def peek: A = data(top-1)
  def isEmpty: Boolean = top == 0
}
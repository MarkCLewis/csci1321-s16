package adt

import scala.reflect.ClassTag


/**
 * @author mlewis
 */
class UnsortedArrayPQ[A : ClassTag](higherP: (A,A) => Boolean)
    extends MyPriorityQueue[A] {
  private var top = 0
  private var data = new Array[A](10)
  
  def enqueue(o: A): Unit = {
    // TODO - grow if too big
    data(top) = o
    top += 1
  }
  def dequeue(): A = {
    val index = indexOfHighestPriority
    val tmp = data(index)
    top -= 1
    data(index) = data(top)
    tmp
  }
  def peek: A = {
    data(indexOfHighestPriority)
  }
  def isEmpty: Boolean = top == 0
  
  private def indexOfHighestPriority:Int = {
    var tmp = 0
    for(i <- 1 until top) {
      if(higherP(data(i), data(tmp))) tmp = i
    }
    tmp
  }
}
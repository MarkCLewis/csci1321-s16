package rmidraw

@remote trait RemoteDrawing {
  def addElement(de:DrawElement)
  def getElements():Seq[DrawElement]
}
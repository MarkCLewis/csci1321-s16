package rmidraw

@remote trait RemoteServer {
  def connect(client: RemoteClient): Map[String, RemoteDrawing]
  def newDrawing(name: String, creator: RemoteClient): Unit
}
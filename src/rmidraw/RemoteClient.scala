package rmidraw

@remote trait RemoteClient {
  def updateAll(drawings: Map[String, RemoteDrawing]): Unit
  def updateDrawing(name: String, elements: Seq[DrawElement]): Unit
}
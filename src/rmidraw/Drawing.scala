package rmidraw

import java.rmi.server.UnicastRemoteObject
import scalafx.scene.image.WritableImage
import scalafx.scene.canvas.Canvas
import scalafx.scene.SnapshotParameters
import java.rmi.RemoteException

class Drawing(val name: String) extends UnicastRemoteObject with RemoteDrawing {
  private var elements = List[DrawElement]()

  def sendUpdate(): Unit = {
    RMIDrawServer.clientList.foreach(c => try {
      c.updateDrawing(name, elements)
    } catch {
      case ex:RemoteException => RMIDrawServer.removeClient(c)
    })
  }

  def addElement(de:DrawElement):Unit = {
    elements ::= de
    sendUpdate()
  }
  
  def getElements():Seq[DrawElement] = elements
}
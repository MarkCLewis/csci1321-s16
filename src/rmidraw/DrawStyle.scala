package rmidraw

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.paint.Color
import scalafx.scene.control.TextInputDialog

trait DrawStyle {
  def mousePressed(x: Double, y: Double): Unit
  def mouseDragged(x: Double, y: Double, color: Color): Option[DrawElement]
  def mouseReleased(x: Double, y: Double, color: Color): Option[DrawElement]
  def drawIntermediate(gc: GraphicsContext, color: Color): Unit
}

object LineDrawStyle extends DrawStyle {
  private var sx = 0.0
  private var sy = 0.0
  def mousePressed(x: Double, y: Double): Unit = {
    sx = x
    sy = y
  }

  def mouseDragged(x: Double, y: Double, color: Color): Option[DrawElement] = {
    val elem = DrawLine(sx, sy, x, y, SColor(color))
    sx = x
    sy = y
    Some(elem)
  }

  def mouseReleased(x: Double, y: Double, color: Color): Option[DrawElement] = {
    val elem = DrawLine(sx, sy, x, y, SColor(color))
    sx = 0.0
    sy = 0.0
    Some(elem)
  }

  def drawIntermediate(gc: GraphicsContext, color: Color): Unit = {
  }
}

object RectangleDrawStyle extends DrawStyle {
  private var sx = 0.0
  private var sy = 0.0
  private var cx = 0.0
  private var cy = 0.0

  def mousePressed(x: Double, y: Double): Unit = {
    sx = x
    sy = y
    cy = x
    cy = y
  }

  def mouseDragged(x: Double, y: Double, color: Color): Option[DrawElement] = {
    cx = x
    cy = y
    None
  }

  def mouseReleased(x: Double, y: Double, color: Color): Option[DrawElement] = {
    val elem = DrawRect(sx min cx, sy min cy, (sx - cx).abs, (sy - cy).abs, SColor(color))
    sx = 0.0
    sy = 0.0
    cx = 0.0
    cy = 0.0
    Some(elem)
  }

  def drawIntermediate(gc: GraphicsContext, color: Color): Unit = {
    gc.fill = color
    gc.fillRect(sx min cx, sy min cy, (sx - cx).abs, (sy - cy).abs)
  }
}

object OvalDrawStyle extends DrawStyle {
  private var sx = 0.0
  private var sy = 0.0
  private var cx = 0.0
  private var cy = 0.0

  def mousePressed(x: Double, y: Double): Unit = {
    sx = x
    sy = y
    cy = x
    cy = y
  }

  def mouseDragged(x: Double, y: Double, color: Color): Option[DrawElement] = {
    cx = x
    cy = y
    None
  }

  def mouseReleased(x: Double, y: Double, color: Color): Option[DrawElement] = {
    val elem = DrawOval(sx min cx, sy min cy, (sx - cx).abs, (sy - cy).abs, SColor(color))
    sx = 0.0
    sy = 0.0
    cx = 0.0
    cy = 0.0
    Some(elem)
  }

  def drawIntermediate(gc: GraphicsContext, color: Color): Unit = {
    gc.fill = color
    gc.fillOval(sx min cx, sy min cy, (sx - cx).abs, (sy - cy).abs)
  }
}

object TextDrawStyle extends DrawStyle {
  def mousePressed(x: Double, y: Double): Unit = {}

  def mouseDragged(x: Double, y: Double, color: Color): Option[DrawElement] = {
    None
  }

  def mouseReleased(x: Double, y: Double, color: Color): Option[DrawElement] = {
    val text = new TextInputDialog
    text.title = "Text"
    text.headerText = "What text do you want?"
    text.contentText = "Text"
    text.showAndWait() match {
      case Some(s) => Some(DrawText(x, y, s, SColor(color)))
      case None => None
    }
  }

  def drawIntermediate(gc: GraphicsContext, color: Color): Unit = {

  }
}
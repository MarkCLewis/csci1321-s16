package rmidraw

import scalafx.scene.canvas.GraphicsContext

sealed trait DrawElement {
  def draw(gc: GraphicsContext): Unit
}

case class DrawLine(x1: Double, y1: Double, x2: Double, y2: Double, color: SColor) extends DrawElement {
  def draw(gc: GraphicsContext): Unit = {
    gc.stroke = color.toColor
    gc.strokeLine(x1, y1, x2, y2)
  }
}

case class DrawRect(x: Double, y: Double, width: Double, height: Double, color: SColor) extends DrawElement {
  def draw(gc: GraphicsContext): Unit = {
    gc.fill = color.toColor
    gc.fillRect(x, y, width, height)
  }
}

case class DrawOval(x: Double, y: Double, width: Double, height: Double, color: SColor) extends DrawElement {
  def draw(gc: GraphicsContext): Unit = {
    gc.fill = color.toColor
    gc.fillOval(x, y, width, height)
  }
}

case class DrawText(x: Double, y: Double, text: String, color: SColor) extends DrawElement {
  def draw(gc: GraphicsContext): Unit = {
    gc.fill = color.toColor
    gc.fillText(text, x, y)
  }
}

package rmidraw

import scalafx.application.JFXApp
import java.rmi.server.UnicastRemoteObject
import java.rmi.registry.LocateRegistry
import java.rmi.Naming

object RMIDrawServer extends UnicastRemoteObject with RemoteServer {
  private var clients:List[RemoteClient] = Nil
  private var drawings = Map[String, Drawing]()
  
  def connect(client: RemoteClient): Map[String, RemoteDrawing] = {
    println("Connection")
    clients ::= client
    drawings
  }
  
  def newDrawing(name: String, creator: RemoteClient): Unit = {
    println(s"New drawing: $name")
    if(!drawings.contains(name)) {
      val drawing = new Drawing(name)
      drawings += name -> drawing
      clients.foreach(_.updateAll(drawings))
      drawing.sendUpdate()
    }
  }
  
  def clientList = clients
  
  def removeClient(c:RemoteClient):Unit = clients = clients.filter(_ != c)
  
  def main(args:Array[String]):Unit = {
    LocateRegistry.createRegistry(1099)
    Naming.rebind("DrawServer", this)
  }
}
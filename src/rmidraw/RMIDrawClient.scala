package rmidraw

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.layout.BorderPane
import scalafx.scene.image.ImageView
import scalafx.scene.image.WritableImage
import java.rmi.Naming
import scalafx.scene.control.Dialog
import java.rmi.server.UnicastRemoteObject
import scalafx.scene.control.TextInputDialog
import scalafx.scene.control.MenuBar
import scalafx.scene.layout.VBox
import scalafx.scene.input.MouseEvent
import scalafx.scene.control.Menu
import scalafx.scene.control.MenuItem
import scalafx.event.ActionEvent
import scalafx.scene.canvas.Canvas
import scalafx.scene.control.ColorPicker
import scalafx.scene.paint.Color
import scalafx.scene.control.ToolBar
import scalafx.scene.control.RadioButton
import scalafx.application.Platform
import scalafx.scene.control.ToggleButton
import scalafx.scene.control.ToggleGroup

case class CanvasRemote(canvas: Canvas, drawing: RemoteDrawing, elements:Seq[DrawElement])

object RMIDrawClient extends JFXApp {
  val hostInput = new TextInputDialog("localhost")
  hostInput.title = "Server Host"
  hostInput.headerText = "What machine is the server running on?"
  hostInput.contentText = "Host name or IP"
  val host = hostInput.showAndWait() match {
    case Some(s) => s
    case None => sys.exit()
  }
  private val server = Naming.lookup(s"rmi://$host/DrawServer") match {
    case rs: RemoteServer => rs
    case _ => throw new IllegalArgumentException("Remote value was not a server.")
  }

  class ClientImpl extends UnicastRemoteObject with RemoteClient {
    def updateAll(drawings: Map[String, RemoteDrawing]): Unit = {
      canvases = for ((n, rd) <- drawings) yield {
        if (canvases.contains(n)) {
          n -> CanvasRemote(canvases(n).canvas, rd, canvases(n).elements)
        } else {
          n -> CanvasRemote(new Canvas(60, 60), rd, Nil)
        }
      }
      updateThumbNails()
      if(canvases.contains(currentDrawing)) drawElements(mainCanvas, canvases(currentDrawing).elements, 1.0)
    }
    def updateDrawing(name: String, elements: Seq[DrawElement]): Unit = {
      val cr = canvases(name)
      canvases = canvases.updated(name, CanvasRemote(cr.canvas, cr.drawing, elements))
      drawElements(canvases(name).canvas, elements, 0.1)
      if (currentDrawing == name) drawElements(mainCanvas, elements, 1.0)
    }
  }

  private var currentDrawing = ""
  private val mainCanvas = new Canvas(600, 600)
  val thumbs = new VBox

  val client = new ClientImpl

  private var canvases = for ((n, rd) <- server.connect(client)) yield {
    n -> CanvasRemote(new Canvas(60, 60), rd, rd.getElements())
  }
  if (canvases.isEmpty) {
    val name = randomString
    currentDrawing = name
    server.newDrawing(name, client)
  } else {
    currentDrawing = canvases.keys.head
    updateThumbNails()
    drawElements(mainCanvas, canvases(currentDrawing).elements, 1.0)
  }

  private var drawStyle:DrawStyle = LineDrawStyle
  private val colorPicker = new ColorPicker(Color.Black)

  mainCanvas.onMousePressed = (e: MouseEvent) => {
    drawStyle.mousePressed(e.x, e.y)
    val gc = mainCanvas.graphicsContext2D
    drawStyle.drawIntermediate(gc, colorPicker.value.value)
  }
  mainCanvas.onMouseDragged = (e: MouseEvent) => {
    drawStyle.mouseDragged(e.x, e.y, colorPicker.value.value).foreach(de => canvases(currentDrawing).drawing.addElement(de))
    val gc = mainCanvas.graphicsContext2D
    drawStyle.drawIntermediate(gc, colorPicker.value.value)
  }
  mainCanvas.onMouseReleased = (e: MouseEvent) => {
    drawStyle.mouseReleased(e.x, e.y, colorPicker.value.value).foreach(de => canvases(currentDrawing).drawing.addElement(de))
    val gc = mainCanvas.graphicsContext2D
    drawStyle.drawIntermediate(gc, colorPicker.value.value)
  }

  stage = new JFXApp.PrimaryStage {
    title = "RMI Draw"
    scene = new Scene(800, 670) {
      val border = new BorderPane
      border.center = mainCanvas
      val menuBar = new MenuBar
      val fileMenu = new Menu("File")
      val newDrawing = new MenuItem("New Drawing")
      newDrawing.onAction = (e: ActionEvent) => server.newDrawing(randomString, client)
      val exitItem = new MenuItem("Exit")
      exitItem.onAction = (e: ActionEvent) => sys.exit(0)
      fileMenu.items = List(newDrawing, exitItem)
      menuBar.menus = List(fileMenu)
      border.top = menuBar
      border.right = thumbs
      val tools = new ToolBar
      val lineButton = new ToggleButton("Line")
      lineButton.onAction = (e:ActionEvent) => drawStyle = LineDrawStyle
      val rectButton = new ToggleButton("Rectangle")
      rectButton.onAction = (e:ActionEvent) => drawStyle = RectangleDrawStyle
      val ovalButton = new ToggleButton("Oval")
      ovalButton.onAction = (e:ActionEvent) => drawStyle = OvalDrawStyle
      val textButton = new ToggleButton("Text")
      textButton.onAction = (e:ActionEvent) => drawStyle = TextDrawStyle
      val group = new ToggleGroup
      group.toggles = List(lineButton, rectButton, ovalButton, textButton)
      lineButton.selected = true
      tools.items = List(colorPicker, lineButton, rectButton, ovalButton, textButton)
      border.bottom = tools
      root = border
    }
  }

  def updateThumbNails(): Unit = {
    Platform.runLater {
      thumbs.children.clear()
      thumbs.children = for ((n, rc) <- canvases) yield {
        drawElements(rc.canvas, canvases(n).elements, 0.1)
        rc.canvas.onMouseClicked = (e:MouseEvent) => {
          currentDrawing = n
          drawElements(mainCanvas, canvases(n).elements, 1.0)
        }
        rc.canvas
      }
    }
  }

  def drawElements(canvas: Canvas, elements: Seq[DrawElement], scale: Double): Unit = {
    val gc = canvas.graphicsContext2D
    gc.fill = Color.White
    gc.fillRect(0, 0, 600, 600)
    gc.save
    gc.scale(scale, scale)
    for (e <- elements.reverse) e.draw(gc)
    gc.restore
  }

  def randomString: String = Array.fill(10)(('a' to 'z')(util.Random.nextInt(26))).mkString
}
package rmidraw

import scalafx.scene.paint.Color

case class SColor(red:Double, green:Double, blue:Double, alpha:Double) {
  def toColor:Color = Color(red, green, blue, alpha)
}

object SColor {
  def apply(c:Color):SColor = SColor(c.red, c.green, c.blue, c.opacity)
}
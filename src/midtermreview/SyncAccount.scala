package midtermreview

/**
 * @author mlewis
 */
class SyncAccount {
  private var _balance = 0
  
  def balance = _balance
  
  def deposit(amount:Int):Unit = synchronized {
    assert(amount > 0)
    _balance += amount
  }
  
  def withdraw(amount:Int):Unit = synchronized {
    assert(amount > 0 && amount <= _balance)
    _balance -= amount
  }
}

object SyncAccount extends App {
  val acc = new SyncAccount
  for(i <- (1 to 1000000).par) acc.deposit(10)
  println(acc.balance)
}
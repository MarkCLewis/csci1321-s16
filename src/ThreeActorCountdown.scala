

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props

/* 
 * @author mlewis
 */
object ThreeActorCountdown extends App {
  case class CountDown(n:Int, next:ActorRef, nextNext:ActorRef)
  
  class CountDownActor extends Actor {
    def receive = {
      case CountDown(i,next,nextNext) =>
        if(i>0) {
          println(i)
          next ! CountDown(i-1, nextNext, self)
        } else {
          context.system.terminate()
        }
    }
  }
  
  val system = ActorSystem("ThreeCounting")
  val actor1 = system.actorOf(Props[CountDownActor],"a1")
  val actor2 = system.actorOf(Props[CountDownActor],"a2")
  val actor3 = system.actorOf(Props[CountDownActor],"a3")
  
  actor1 ! CountDown(10, actor2, actor3)
}
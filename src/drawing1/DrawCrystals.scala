package drawing1

import scalafx.scene.canvas.GraphicsContext
import akka.actor.Actor
import scalafx.scene.image.WritableImage
import akka.actor.ActorSystem
import akka.actor.Props
import scalafx.application.Platform
import scalafx.scene.paint.Color

/**
 * @author mlewis
 */
class DrawCrystals extends Drawable {
  val Width = 600
  val Height = 600
  val NumFloaties = 10
  val img = new WritableImage(Width, Height)

  case object GrowCrystals
  case object Float
  case object Reset
  case class CheckSpot(x: Int, y: Int)
  case class SetContext(gc:GraphicsContext)

  val system = ActorSystem("CrystalSystem")
  val imageActor = system.actorOf(Props(new ImageActor(img)), "ImageActor")

  imageActor ! GrowCrystals

  def draw(gc: GraphicsContext) = {
    imageActor ! SetContext(gc)
  }

  class ImageActor(wi: WritableImage) extends Actor {
    private val writer = wi.pixelWriter
    private val reader = wi.pixelReader.get
    private var gc:Option[GraphicsContext] = None
    for (i <- 1 to NumFloaties) {
      context.actorOf(Props(new FloatyActor), "floaty" + i)
    }
    def receive = {
      case GrowCrystals =>
        writer.setArgb(Height - 1, Width / 2, 0xff000000)
        for (c <- context.children) c ! Float
      case Reset =>
        for (i <- 0 until Width; j <- 0 until Height) writer.setArgb(i, j, 0)
        writer.setArgb(Height - 1, Width / 2, 0xff000000)
      case CheckSpot(x, y) =>
        if (adjacent(x, y)) {
          writer.setColor(x, y, Color.Black)
          gc.foreach { g => Platform.runLater { g.drawImage(img,0,0) } }
          sender ! Reset
        }
        sender ! Float
      case SetContext(newGC) => gc = Option(newGC)
    }

    def adjacent(x: Int, y: Int): Boolean = {
      isSet(x + 1, y) || isSet(x - 1, y) || isSet(x, y + 1) || isSet(x, y - 1)
    }

    def isSet(x: Int, y: Int): Boolean = {
      x >= 0 && x < Width && y >= 0 && y < Height && reader.getArgb(x, y) == 0xff000000
    }
  }

  class FloatyActor extends Actor {
    private var x = util.Random.nextInt(Width)
    private var y = 0
    def receive = {
      case Float =>
        val dx = util.Random.nextInt(3) - 1
        val dy = util.Random.nextInt(3) - 1
        if (x + dx >= 0 && x + dx < Width && y + dy >= 0 && y + dy < Height) {
          x += dx
          y += dy
        }
        sender ! CheckSpot(x, y)
      case Reset =>
        x = util.Random.nextInt(Width)
        y = 0
    }
  }
}
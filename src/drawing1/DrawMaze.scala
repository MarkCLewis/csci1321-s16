package drawing1

import scalafx.scene.canvas.GraphicsContext

/**
 * @author mlewis
 */
class DrawMaze extends Drawable {
  private val maze = Array(Array(0,1,0,0,0,0,0,0,0,0),
                           Array(0,1,0,1,1,0,1,1,1,0),
                           Array(0,1,0,0,1,0,1,0,1,0),
                           Array(0,1,1,0,1,0,1,0,1,0),
                           Array(0,0,0,0,1,0,1,0,0,0),
                           Array(0,1,1,1,1,0,1,1,1,1),
                           Array(0,1,0,0,0,0,0,1,0,0),
                           Array(0,1,0,1,0,1,0,0,0,0),
                           Array(0,1,0,1,0,1,1,1,1,1),
                           Array(0,0,0,1,0,0,0,0,0,0))
  
  def draw(gc:GraphicsContext):Unit = {
    
  }
  
  def breadthFirstShortestPath(sx:Int, sy:Int, ex:Int, ey:Int):Int = {
    val offsets = Array((1,0), (-1,0), (0,-1), (0,1))
    val visited = collection.mutable.Set[(Int,Int)]()
    val queue = collection.mutable.Queue[(Int,Int,Int)]()
    queue.enqueue((sx,sy,0))
    var answer = -1
    while(queue.nonEmpty && answer < 0) {
      val (x, y, steps) = queue.dequeue()
      for((ox,oy) <- offsets) {
        if(x+ox>=0 && x+ox<maze.length &&
            y+oy>=0 && y+oy<maze(x).length && 
            maze(x)(y)==0 && !visited((x+ox,y+oy))) {
          if(x+ox == ex && y+oy == ey) {
            answer = steps+1
          }
          queue.enqueue((x+ox,y+oy,steps+1))
          visited((x+ox,y+oy)) = true
        }
      }
    }
    answer
  }
}

object DrawMaze extends App {
  val dmaze = new DrawMaze
  println(dmaze.breadthFirstShortestPath(0, 0, 9, 9))
}
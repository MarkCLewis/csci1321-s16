package drawing1


import scalafx.scene.canvas._

/**
 * @author mlewis
 */
abstract class Drawable extends Serializable {
  def draw(gc:GraphicsContext)
}
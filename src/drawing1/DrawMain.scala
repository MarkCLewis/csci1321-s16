package drawing1

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.control.TreeView
import scalafx.scene.control.ScrollPane
import scalafx.scene.canvas.Canvas
import scalafx.scene.layout.BorderPane
import scalafx.scene.control.SplitPane
import scalafx.scene.paint.Color
import scalafx.geometry.Orientation
import scalafx.scene.control.TextField
import scalafx.scene.control.TextArea
import scalafx.scene.control.MenuBar
import scalafx.scene.control.Menu
import scalafx.scene.control.MenuItem
import scalafx.event.ActionEvent
import java.io.FileOutputStream
import java.io.ObjectOutputStream
import java.io.BufferedOutputStream
import scalafx.stage.FileChooser
import java.io.FileInputStream
import java.io.BufferedInputStream
import java.io.ObjectInputStream
import scalafx.scene.canvas.GraphicsContext
import scala.collection.mutable
import java.net.ServerSocket
import java.io.PrintStream
import scala.concurrent.Future
import java.io.InputStream
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * @author mlewis
 */
object DrawMain extends JFXApp {
  var drawing = new Drawing(List(new DrawRectangle(100, 100, 200, 50, Color.Blue),
    //    new DrawCircle(100, 100, 50, Color.Red),
    new DrawRectangle(200, 300, 25, 25, Color.Green)))
  //            new DrawCrystals))
  val users = mutable.Buffer[ChatUser]()

  stage = new JFXApp.PrimaryStage {
    title = "Let's Draw Pictures!"
    scene = new Scene(800, 800) {
      val tree = new TreeView
      val propPane = new ScrollPane
      val canvas = new Canvas(600, 600)
      val gc = canvas.graphicsContext2D
      val borderPane = new BorderPane
      val splitPane = new SplitPane
      splitPane.orientation = Orientation.Vertical
      splitPane.items += tree
      splitPane.items += propPane
      borderPane.left = splitPane
      val commandBorder = new BorderPane
      val commandField = new TextField
      val commandArea = new TextArea
      commandArea.editable = false
      val commandProcessor = new Commands(commandArea.text)
      commandField.onAction = (e: ActionEvent) => {
        val response = commandProcessor.process(commandField.text.value)
        commandArea.text = commandArea.text.value + "\n" + response
        commandField.text = ""
      }
      commandBorder.center = commandArea
      commandBorder.top = commandField
      val centerBorder = new BorderPane
      centerBorder.center = canvas
      centerBorder.bottom = commandBorder
      borderPane.center = centerBorder
      val menuBar = new MenuBar
      val fileMenu = new Menu("File")
      val openItem = new MenuItem("Open")
      openItem.onAction = (e: ActionEvent) => openDrawing(gc)
      val saveItem = new MenuItem("Save")
      saveItem.onAction = (e: ActionEvent) => saveDrawing()
      val exitItem = new MenuItem("Exit")
      exitItem.onAction = (e: ActionEvent) => sys.exit(0)
      fileMenu.items = List(openItem, saveItem, exitItem)
      menuBar.menus = List(fileMenu)
      borderPane.top = menuBar
      gc.fill = Color.White
      gc.fillRect(0, 0, canvas.width.value, canvas.height.value)
      drawing.draw(gc)
      root = borderPane
      Future {
        runChatServer()
      }
      Future {
        monitorChat()
      }
    }
  }

  private def saveDrawing() = {
    val chooser = new FileChooser
    val file = chooser.showSaveDialog(null)
    if (file != null) {
      val oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))
      oos.writeObject(drawing)
      oos.close()
    }
  }

  private def openDrawing(gc: GraphicsContext) = {
    val chooser = new FileChooser
    val file = chooser.showOpenDialog(null)
    if (file != null) {
      val ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))
      drawing = ois.readObject() match {
        case d: Drawing =>
          d.draw(gc)
          d
        case _ => throw new IllegalArgumentException("File didn't contain a drawing.")
      }
      ois.close()
    }
  }

  private def runChatServer(): Unit = {
    val ss = new ServerSocket(4444)
    while (true) {
      val sock = ss.accept()
      val ps = new PrintStream(sock.getOutputStream)
      val is = sock.getInputStream
      Future {
        val name = blockingRead(is).trim
        users += ChatUser(name, sock, is, ps)
      }
    }
  }

  private def monitorChat(): Unit = {
    while (true) {
      for (u <- users) {
        nonblockingRead(u.is).foreach(input => {
          val msg = u.name + " said \"" + input
          for (u2 <- users; if u2 != u) u2.ps.println(msg)
        })
      }
      Thread.sleep(100)
    }
  }

  def blockingRead(is: InputStream): String = {
    var input: Option[String] = None
    while (input.isEmpty) {
      input = nonblockingRead(is)
      Thread.sleep(100)
    }
    input.get
  }

  def nonblockingRead(is: InputStream): Option[String] = {
    if (is.available() > 0) {
      val buf = Array.fill(is.available())(0.toByte) // new Array[Byte](is.available)
      is.read(buf)
      Some(new String(buf))
    } else None
  }
}
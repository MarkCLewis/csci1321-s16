package drawing1

import java.net.Socket
import java.io.PrintStream
import java.io.InputStream

/**
 * @author mlewis
 */
case class ChatUser(name:String, sock:Socket, is:InputStream, ps:PrintStream) {
  
}


/*
 * @author mlewis
 */
package drawing1

class Complex(val real: Double, val imag: Double) {
  def +(c: Complex): Complex = new Complex(real + c.real, imag + c.imag)
  def -(c: Complex): Complex = new Complex(real - c.real, imag - c.imag)
  def *(x: Double): Complex = new Complex(real * x, imag * x)
  def /(x: Double): Complex = new Complex(real / x, imag / x)
  def *(c: Complex): Complex = new Complex(real * c.real - imag * c.imag, real * c.imag + imag * c.real)
  def mag: Double = math.sqrt(real * real + imag * imag)

  def unary_-(): Complex = new Complex(-real, -imag)

  override def toString():String = s"$real + i*$imag"
}

object Complex {
  def apply(r: Double, i: Double): Complex = new Complex(r, i)

  val c = Complex(6, 7)
  
  def main(args:Array[String]):Unit = {
    println(c)
  }

}

package drawing1

import java.io.PrintStream
import java.io.InputStream
import java.net.Socket
import scalafx.beans.property.StringProperty
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * @author mlewis
 */
class Commands(text: StringProperty) {
  private var sock: Option[Socket] = None
  private var is: Option[InputStream] = None
  private var ps: Option[PrintStream] = None

  val commands = Map[String, String => Any](
    "echo" -> (args => args.trim),
    "add" -> (args => args.trim.split(" ").map(_.toDouble).sum),
    "connect" -> (args => chatConnect(args.trim)),
    "disconnect" -> (args => sock.foreach(s => {
      s.close()
      sock = None
      is = None
      ps = None
    })),
    "chat" -> (args => sendChat(args.trim)))

  def process(com: String): Any = {
    val (f, args) = {
      val i = com.indexOf(" ")
      if (i < 0) (com, "")
      else com.splitAt(i)
    }
    if (commands.contains(f)) commands(f)(args)
    else "Bad Command"
  }

  def chatConnect(args: String): String = {
    if (sock.isEmpty) {
      val index = args.indexOf(" ")
      if (index < 0) {
        "You need to specify a name and a host to connect to."
      } else {
        val (name, host) = args.splitAt(index)
        val s = new Socket(host.trim, 4444)
        sock = Some(s)
        is = Some(s.getInputStream)
        Future {
          while (is.nonEmpty) {
            is.foreach(stream => {
              DrawMain.nonblockingRead(stream).foreach(msg => text.value = text.value + msg.trim + "\n")
            })
            Thread.sleep(100)
          }
        }
        ps = Some(new PrintStream(s.getOutputStream))
        ps.get.println(name)
        "Connection made."
      }
    } else "You are already connected. Please disconnect."
  }

  def sendChat(args: String): String = {
    ps match {
      case None => "You must connect to chat."
      case Some(stream) =>
        stream.println(args)
        "Chat sent."
    }
  }
}
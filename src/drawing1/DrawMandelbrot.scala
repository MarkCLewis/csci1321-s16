package drawing1

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.paint.Color
import scalafx.application.Platform
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * @author mlewis
 */
class DrawMandelbrot extends Drawable {
  val xmin = -1.5
  val xmax = 0.5
  val ymin = -1.0
  val ymax = 1.0
  val maxIters = 10000
  val width = 600
  val height = 600

  def mandelCount(c: Complex): Int = {
    var cnt = 0
    var z = Complex(0, 0)
    while (cnt < maxIters && z.mag < 4) {
      z = z * z + c
      cnt += 1
    }
    cnt
  }

  def draw(gc: GraphicsContext) = {
    drawFuture(gc)
  }

  private def drawFuture(gc: GraphicsContext) = {
    val start = System.nanoTime()
    val futures = for (i <- 0 until width) yield Future {
      for (j <- 0 until height) yield {
        val x = xmin + i * (xmax - xmin) / width
        val y = ymin + j * (ymax - ymin) / height
        val cnt = mandelCount(Complex(x, y))
        cnt
      }
    }
    for (i <- futures.indices) {
      futures(i).foreach(col =>
      Platform.runLater {
        for (j <- 0 until height) {
          val cnt = col(j)
            gc.fill = if (cnt == maxIters) Color.Black else {
              val scale = 10 * math.sqrt(cnt.toDouble / maxIters) min 1.0
              Color(scale, 0, 0, 1)
            }
            gc.fillRect(i, j, 1, 1)
          }
        })
    }
    println((System.nanoTime() - start) / 1e9)
  }

  private def drawPar(gc: GraphicsContext) = {
    val start = System.nanoTime()
    for (i <- (0 until width).par) {
      for (j <- 0 until height) {
        val x = xmin + i * (xmax - xmin) / width
        val y = ymin + j * (ymax - ymin) / height
        val cnt = mandelCount(Complex(x, y))
        Platform.runLater {
          gc.fill = if (cnt == maxIters) Color.Black else {
            val scale = 10 * math.sqrt(cnt.toDouble / maxIters) min 1.0
            Color(scale, 0, 0, 1)
          }
          gc.fillRect(i, j, 1, 1)
        }
      }
    }
    println((System.nanoTime() - start) / 1e9)
  }
}
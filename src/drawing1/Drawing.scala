package drawing1

import scalafx.scene.canvas.GraphicsContext

/**
 * @author mlewis
 */
class Drawing(private var shapes:List[Drawable] = Nil) extends Drawable {
  def draw(gc:GraphicsContext) = {
    shapes.foreach(_.draw(gc))
  }

}
package mud

import akka.actor.ActorSystem

object Main extends App {
  val system = ActorSystem("MUDSystem")  // This is the only actor system in the program
  
  // ...
}
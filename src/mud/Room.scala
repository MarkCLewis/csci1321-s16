package mud

import akka.actor.Actor
import akka.actor.ActorRef
import scala.xml.Node

class Item(name:String) // I'm putting this here to make this file compile. It really goes in a different file.

class Room(val keyword: String, val name: String, val desc: String, private var items: List[Item], val exitKeys: Array[String]) extends Actor {
  private var characters:List[ActorRef] = Nil
  private var exits:Array[Option[ActorRef]] = null
  
  import Room._
  
  def receive = {
    case LinkExits(rooms) =>
      exits = exitKeys.map(s => if(s.isEmpty()) None else Some(rooms(s)))
    case GetItem(itemName) =>
      getItem(itemName) match {
        case Some(item) => sender ! Player.GotItem(item)
        case None => sender ! Player.ItemNotFound
      }
    case TakeExit(dir) =>
      getExit(dir) match {
        case Some(room) =>
          removePlayer(sender)
          sender ! Player.MovedRooms(room)
        case None => 
          sender ! Player.InvalidDirection
      }
    case EnterRoom(player) =>
      addPlayer(player)
  }

  def printDescription(): Unit = ???
  def getExit(dir: Int): Option[ActorRef] = exits(dir)
  def getItem(itemName: String): Option[Item] = ???
  def dropItem(item: Item): Unit = ???
  def addPlayer(player:ActorRef) = ???
  def removePlayer(player:ActorRef) = ???

}

object Room {
  case class LinkExits(rooms:Map[String, ActorRef])
  case class GetItem(itemName:String)
  case class TakeExit(dir:Int)
  case class EnterRoom(player:ActorRef)
  
  def apply(n:Node):Room = {
    ???
  }
}
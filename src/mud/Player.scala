package mud

import akka.actor.Actor
import akka.actor.ActorRef

class Player extends Actor {
  def receive = {
    case _ => ???
  }
  
  // ...
}

object Player {
  case object ItemNotFound
  case class GotItem(item:Item)
  case object InvalidDirection
  case class MovedRooms(room:ActorRef)
  case class Print(message:String)
}
package mud

import akka.actor.Actor
import akka.actor.ActorRef

class ActivityManager extends Actor {
  import ActivityManager._
  
//  private val pq = new PriorityQueue[Event]
  private var currentTime = 0
  def receive = {
    case ScheduleEvent(delta:Int, message:Any) =>
//      pq.enqueue(Event(currentTime+delta, sender, message))
    case DoEvents =>
      // while there are events on the priority queue that occur at or before the current time, process them by
      // sending the message back to the sender.
      currentTime += 1
  }
}

object ActivityManager {
  // This is the type for the events added to the queue.
  case class Event(time:Int, sender:ActorRef, message:Any)

  // Tells the ActivityManager to schedule an event at delta after the current time to send back the specified message.
  case class ScheduleEvent(delta:Int, responseMessage:Any)
  // Tells the activity manager to check for events that need to be processed.
  case object DoEvents 
}
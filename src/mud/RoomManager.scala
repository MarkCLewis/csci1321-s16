package mud

import akka.actor.Actor
import akka.actor.ActorRef
import scala.xml.XML
import akka.actor.Props

class RoomManager extends Actor {
  val rooms:Map[String, ActorRef] = {
    XML.loadFile("map.xml").map(n => {
      (n \ "@keyword").text -> context.actorOf(Props(Room(n)))
    }).toMap
  }
  
  context.children.foreach(room => room ! Room.LinkExits(rooms))
  
  import RoomManager._
  
  def receive = {
    case InitialEnterRoom(player, roomKey) =>
      player ! Player.MovedRooms(rooms(roomKey))
    case FindShortestPath(cur,dest) =>
      val curString = ??? // reverse look-up of room key
      val path = shortestPath(curString, dest)
      sender ! Player.Print(path.mkString(", "))
  }
  
  def shortestPath(cur:String, dest:String):List[String] = {
    ???
  }
}

object RoomManager {
  case class InitialEnterRoom(player:ActorRef, roomKey:String)
  case class FindShortestPath(cur:ActorRef, dest:String)
}
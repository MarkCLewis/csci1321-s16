package drawing2

import scalafx.scene.canvas.GraphicsContext
import collection.mutable.Buffer

/**
 * @author mlewis
 */
class Drawing extends Drawable {
  private val shapes = Buffer[Drawable]()
  
  def clear():Unit = shapes.clear()

  def draw(gc:GraphicsContext):Unit = {
    for(s <- shapes) s.draw(gc)
  }
  
  def remove(s:Drawable):Boolean = {
    if(shapes.contains(s)) {
      shapes -= s
      true
    } else false
  }
  
  def add(s:Drawable):Unit = shapes += s
}
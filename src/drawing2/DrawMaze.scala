package drawing2

import scalafx.scene.canvas.GraphicsContext

/**
 * @author mlewis
 */
class DrawMaze extends Drawable {
  private val maze = Array(Array(0,1,0,0,0,0,0,0,0,0),
                           Array(0,1,0,1,1,0,1,1,1,0),
                           Array(0,1,0,0,1,0,1,0,1,0),
                           Array(0,1,1,0,1,0,1,0,1,0),
                           Array(0,0,0,0,1,0,1,0,0,0),
                           Array(0,1,1,1,1,0,1,1,1,1),
                           Array(0,1,0,0,0,0,0,1,0,0),
                           Array(0,1,0,1,0,1,0,0,0,0),
                           Array(0,1,0,1,0,1,1,1,1,1),
                           Array(0,0,0,1,0,0,0,0,0,0))
  
  def draw(gc:GraphicsContext):Unit = {
    
  }
  
  def breadthFirstShortestPath(sx:Int, sy:Int, ex:Int, ey:Int):Int = {
    val offsets = Array((1,0), (-1,0), (0,1), (0,-1))
    val queue = collection.mutable.Queue[(Int,Int,Int)]()
    val visited = collection.mutable.Set[(Int,Int)]()
    queue.enqueue((sx,sy,0))
    visited((sx,sy)) = true
    while(queue.nonEmpty) {
      val (x,y,steps) = queue.dequeue()
      for((ox,oy) <- offsets) {
        if(x+ox==ex && y+oy==ey) {
          return steps+1
        }
        if(x+ox>=0 && x+ox<maze.length &&
            y+oy>=0 && y+oy<maze(x).length &&
            maze(x+ox)(y+oy)==0 && !visited((x+ox,y+oy))) {
          queue.enqueue((x+ox,y+oy,steps+1))
          visited((x+ox,y+oy)) = true
        }
      }
    }
    -1
  }
}

object DrawMaze extends App {
  val dmaze = new DrawMaze
  println(dmaze.breadthFirstShortestPath(0, 0, 9, 9))
}
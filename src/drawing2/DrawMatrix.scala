package drawing2

import scalafx.scene.canvas.GraphicsContext
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props
import scala.concurrent.duration._
import scalafx.scene.paint.Color
import scalafx.application.Platform

/**
 * @author mlewis
 */
class DrawMatrix extends Drawable {
  val Width = 600
  val Height = 600

  case object Update

  private var g: Option[GraphicsContext] = None

  val system = ActorSystem("MatrixSystem")
  val matrixActor = system.actorOf(Props(new TheMatrix), "TheMatrix")

  implicit val ec = system.dispatcher

  system.scheduler.schedule(0.seconds, 50.millis, matrixActor, Update)

  def draw(gc: GraphicsContext) = {
    g = Option(gc)
  }

  class TheMatrix extends Actor {
    def receive = {
      case Update =>
        g.foreach(gc => Platform.runLater {
          gc.fill = Color.Black
          gc.fillRect(0, 0, Width, Height)
        })
        for (c <- context.children) c ! Update
        context.actorOf(Props(new SymbolCascade))
    }
  }

  class SymbolCascade extends Actor {
    val x = util.Random.nextInt(Width)
    var y = util.Random.nextInt(Height + Height / 2) - Height / 2
    context.actorOf(Props(new SingleSymbol(x, y)))
    def receive = {
      case Update =>
        if (context.children.isEmpty) context.stop(self)
        else {
          if(y < Height+50) context.actorOf(Props(new SingleSymbol(x, y)))
          for (c <- context.children) c ! Update
          y += 20
        }
    }
  }

  class SingleSymbol(x: Int, y: Int) extends Actor {
    val c = util.Random.nextInt(Char.MaxValue).toChar.toString
    var alpha = 1.0
    def receive = {
      case Update =>
        if(alpha > 0.0) {
          val a = alpha
          g.foreach(gc => Platform.runLater {
            gc.fill = Color(0,1,0,a)
            gc.fillText(c, x, y)
          })
        } else {
          context.stop(self)
        }
        alpha -= 0.05
    }
  }
}
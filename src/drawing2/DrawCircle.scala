package drawing2

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.paint.Color

/**
 * @author mlewis
 */
class DrawCircle(
    x: Double,
    y: Double,
    radius: Double,
    color: Color) extends Drawable {
  private var red = color.red
  private var green = color.green
  private var blue = color.blue
  private var alpha = color.opacity

  override def draw(gc: GraphicsContext): Unit = {
    gc.fill = Color(red, green, blue, alpha)
    gc.fillOval(x - radius, y - radius, radius * 2, radius * 2)
  }
}
package drawing2

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.control.ScrollPane
import scalafx.scene.control.SplitPane
import scalafx.scene.control.TreeView
import scalafx.scene.canvas.Canvas
import scalafx.geometry.Orientation
import scalafx.scene.paint.Color
import scalafx.scene.control.TextField
import scalafx.scene.control.TextArea
import scalafx.scene.layout.BorderPane
import scalafx.scene.control.Menu
import scalafx.scene.control.MenuBar
import scalafx.scene.control.MenuItem
import scalafx.event.ActionEvent
import scalafx.stage.FileChooser
import java.io.FileOutputStream
import java.io.ObjectOutputStream
import java.io.BufferedOutputStream
import java.io.FileInputStream
import java.io.BufferedInputStream
import java.io.ObjectInputStream
import scalafx.scene.canvas.GraphicsContext
import java.net.Socket
import java.io.PrintStream
import java.io.InputStream
import scala.collection.mutable
import java.net.ServerSocket
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * @author mlewis
 */
object DrawMain extends JFXApp {
  private var drawing = new Drawing()
  drawing.add(new DrawRectangle(100, 100, 200, 50, Color.BlanchedAlmond))
  drawing.add(new DrawCircle(100, 100, 50, Color.Chartreuse))
  drawing.add(new DrawCircle(200, 300, 75, Color.Peru))
  //  drawing.add(new DrawMatrix)
  
  case class Artist(name:String, sock:Socket, is:InputStream, ps:PrintStream)
  val artists = mutable.Buffer[Artist]()
  
  stage = new JFXApp.PrimaryStage {
    title = "Let's Draw Pictures!"
    scene = new Scene(800, 850) {
      val borderPane = new BorderPane
      val topSplit = new SplitPane
      val bottomSplit = new SplitPane
      bottomSplit.orientation = Orientation.Vertical
      val tree = new TreeView
      val props = new ScrollPane
      val canvas = new Canvas(600, 600)
      val gc = canvas.graphicsContext2D
      val commandField = new TextField
      val commandArea = new TextArea
      val commandBorder = new BorderPane
      val commandProcessor = new Commands(commandArea.text)
      commandField.onAction = (e:ActionEvent) => {
        val result = commandProcessor.process(commandField.text.value)
        commandArea.text = commandArea.text.value +"\n"+result
        commandField.text = ""
      }
      commandBorder.center = commandArea
      commandBorder.bottom = commandField
      val rightBorder = new BorderPane
      rightBorder.center = canvas
      rightBorder.bottom = commandBorder
      bottomSplit.items += tree
      bottomSplit.items += props
      topSplit.items += bottomSplit
      topSplit.items += rightBorder
      val menuBar = new MenuBar
      val fileMenu = new Menu("File")
      val openItem = new MenuItem("Open")
      openItem.onAction = (e: ActionEvent) => openDrawing(gc)
      val saveItem = new MenuItem("Save")
      saveItem.onAction = (e: ActionEvent) => saveDrawing()
      val exitItem = new MenuItem("Exit")
      exitItem.onAction = (e: ActionEvent) => sys.exit(0)
      fileMenu.items = List(openItem, saveItem, exitItem)
      menuBar.menus = List(fileMenu)
      borderPane.top = menuBar
      borderPane.center = topSplit
      root = borderPane
      gc.fill = Color.White
      gc.fillRect(0, 0, canvas.width.value, canvas.height.value)
      drawing.draw(gc)
      Future {
        runChatServer()
      }
      Future {
        monitorChat()
      }
    }
  }

  def saveDrawing() = {
    val chooser = new FileChooser
    val file = chooser.showSaveDialog(null)
    if (file != null) {
      val oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))
      oos.writeObject(drawing)
      oos.close()
    }
  }

  def openDrawing(gc:GraphicsContext) = {
    val chooser = new FileChooser
    val file = chooser.showOpenDialog(null)
    if (file != null) {
      val ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))
      drawing = ois.readObject match {
        case d:Drawing =>
          d.draw(gc)
          d
        case _ => throw new IllegalArgumentException("The file didn't have a drawing.")
      }
      ois.close()
    }
  }
  
  def runChatServer():Unit = {
    val ss = new ServerSocket(4444)
    while(true) {
      val sock = ss.accept()
      val is = sock.getInputStream
      val ps = new PrintStream(sock.getOutputStream)
      Future {
        val name = blockingRead(is).trim
        artists += Artist(name,sock,is,ps)
      }
    }
  }
  
  private def monitorChat(): Unit = {
    while (true) {
      for (a <- artists) {
        nonblockingRead(a.is).foreach(input => {
          val msg = a.name + " said \"" + input
          for (a2 <- artists; if a2 != a) a2.ps.println(msg)
        })
      }
      Thread.sleep(100)
    }
  }

  def blockingRead(is:InputStream):String = {
    var input:Option[String] = None
    while(input.isEmpty) {
      input = nonblockingRead(is)
      Thread.sleep(100)
    }
    input.get
  }
  
  def nonblockingRead(is:InputStream):Option[String] = {
    if(is.available() > 0) {
      val buf = Array.fill(is.available())(0.toByte) // new Array[Byte](is.available)
      is.read(buf)
      Some(new String(buf))
    } else None
  }
}
package drawing2

import scalafx.scene.canvas.GraphicsContext

/**
 * @author mlewis
 */
abstract class Drawable extends Serializable {
  def draw(gc:GraphicsContext):Unit
}
package drawing2

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.paint.Color

/**
 * @author mlewis
 */
class DrawRectangle(
    x: Double,
    y: Double,
    width: Double,
    height: Double,
    color: Color) extends Drawable {
  private var red = color.red
  private var green = color.green
  private var blue = color.blue
  private var alpha = color.opacity

  override def draw(gc: GraphicsContext) = {
    gc.fill = Color(red, green, blue, alpha)
    gc.fillRect(x, y, width, height)
  }
}
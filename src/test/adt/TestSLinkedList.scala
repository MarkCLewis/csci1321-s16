package test.adt

import adt.SLinkedList
import org.junit.Test
import org.junit.Assert._


/**
 * @author mlewis
 */
class TestSLinkedList {
  @Test def lengthZeroAtStart:Unit = {
    val lst = new SLinkedList[Int]()
    assertEquals(0, lst.length)
  }
  
  @Test def oneAfterAdd:Unit = {
    val lst = new SLinkedList[Int]()
    lst.add(5,0)
    assertEquals(1, lst.length)
    assertEquals(5, lst(0))
  }

  @Test def oneThree:Unit = {
    val lst = new SLinkedList[Int]()
    lst.add(7,0)
    lst.add(5,1)
    lst.add(3,0)
    assertEquals(3, lst.length)
    assertEquals(3, lst(0))
    assertEquals(7, lst(1))
    assertEquals(5, lst(2))
  }

  @Test def removes:Unit = {
    val lst = new SLinkedList[Int]()
    lst.add(7,0)
    lst.add(5,1)
    lst.add(3,0)
    assertEquals(7, lst.remove(1))
    assertEquals(3, lst(0))
    assertEquals(5, lst(1))
    assertEquals(3, lst.remove(0))
    assertEquals(5, lst(0))
  }
}
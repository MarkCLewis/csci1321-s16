package test.adt

import org.junit.Test
import org.junit.Assert._
import adt.SortedLinkedListPQ
import adt.UnsortedArrayPQ
import adt.UnsortedLinkedListPQ

/**
 * @author mlewis
 */
class TestPriorityQueue {
  @Test def emptyOnCreate:Unit = {
    val pq = new SortedLinkedListPQ[Int](_ < _)
    assertTrue(pq.isEmpty)
  }
  @Test def add1AndRemove:Unit = {
    val pq = new SortedLinkedListPQ[Int](_ < _)
    pq.enqueue(5)
    assertEquals(5,pq.peek)
    assertEquals(5,pq.dequeue())
  }
  @Test def add3AndRemove:Unit = {
    val pq = new SortedLinkedListPQ[Int](_ < _)
    pq.enqueue(5)
    assertEquals(5,pq.peek)
    pq.enqueue(3)
    assertEquals(3,pq.peek)
    pq.enqueue(7)
    assertEquals(3,pq.peek)
    assertEquals(3,pq.dequeue())
    assertEquals(5,pq.dequeue())
    assertEquals(7,pq.dequeue())
  }
  @Test def add5AndRemove:Unit = {
    val pq = new SortedLinkedListPQ[Int](_ < _)
    pq.enqueue(5)
    pq.enqueue(3)
    pq.enqueue(9)
    pq.enqueue(2)
    pq.enqueue(4)
    assertEquals(2,pq.dequeue())
    assertEquals(3,pq.dequeue())
    assertEquals(4,pq.dequeue())
    assertEquals(5,pq.dequeue())
    assertEquals(9,pq.dequeue())
  }
}